
## Users-crud Project

Demo API REST project to create users


### Stack

* Spring boot 3.0 / Java 17
* Maven 3.8.1
* Hibernate 6.1.5
* H2 2.1


## Getting Started


### 1. Install
1.1. Clone repo
   ```sh
   git clone git@gitlab.com:niko-afv/users-crud-api-spring-boot.git
   ```
1.2. Enter to folder
   ```sh
   cd users-crud-api-spring-boot
   ```
1.3. Install dependencies
   ```sh
   mvn clean install
   ```

### 2. Run
2.1. Run Project
   ```sh
   mvn spring-boot:run
   ```
