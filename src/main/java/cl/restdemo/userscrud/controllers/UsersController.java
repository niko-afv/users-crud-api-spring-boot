package cl.restdemo.userscrud.controllers;


import cl.restdemo.userscrud.exceptions.EmailAlreadyExistsException;
import cl.restdemo.userscrud.models.User;
import cl.restdemo.userscrud.services.UsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

@RestController
@RequestMapping("/api/users")
public class UsersController {

    private UsersService usersService;

    @Autowired
    public UsersController(UsersService usersService) {
        this.usersService = usersService;
    }

    @PostMapping()
    public User users(@RequestBody User user) {
        if( true) throw new EmailAlreadyExistsException("Email ya existe");

        return this.usersService.createUser(user);
    }
}
