package cl.restdemo.userscrud.services.impl;

import cl.restdemo.userscrud.models.User;
import cl.restdemo.userscrud.repositories.UsersRepository;
import cl.restdemo.userscrud.services.UsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UsersServiceImpl implements UsersService {

    private UsersRepository usersRepository;

    @Autowired
    public UsersServiceImpl(UsersRepository usersRepository) {
        this.usersRepository = usersRepository;
    }

    public User createUser(User user) {
        return usersRepository.save(user);
    }

    public boolean userAlreadyExists(String email) {
        return usersRepository.existsByEmail(email);
    }
}
