package cl.restdemo.userscrud.services;

import cl.restdemo.userscrud.models.User;

public interface UsersService {
    public User createUser(User user);

    public boolean userAlreadyExists(Integer userId);
}
