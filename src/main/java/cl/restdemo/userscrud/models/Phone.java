package cl.restdemo.userscrud.models;


import com.fasterxml.jackson.annotation.JsonBackReference;
import jakarta.persistence.*;

@Entity
@Table(name="phones")
public class Phone {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    private String number;
    private Integer cityCode;
    private Integer countryCode;

    @ManyToOne
    @JoinColumn(name="user_id")
    @JsonBackReference
    private User user;


    public Phone(String number, Integer cityCode, Integer countryCode, User userId) {
        this.number = number;
        this.cityCode = cityCode;
        this.countryCode = countryCode;
        this.user = userId;
    }

    public Phone(User userId) {
        this.user = userId;
    }

    public Phone() {
        this.number = "0";
        this.cityCode = 0;
        this.countryCode = 0;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public Integer getCityCode() {
        return cityCode;
    }

    public void setCityCode(Integer cityCode) {
        this.cityCode = cityCode;
    }

    public Integer getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(Integer countryCode) {
        this.countryCode = countryCode;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
