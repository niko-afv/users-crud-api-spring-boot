package cl.restdemo.userscrud.models;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.JsonProperty;
import jakarta.persistence.*;
import org.hibernate.annotations.CreationTimestamp;

import java.util.Date;
import java.util.List;


@Entity
@Table(name="users")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    private String name;
    private String email;
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private String password;
    @OneToMany(mappedBy = "user", cascade = {CascadeType.ALL})
    @JsonManagedReference
    private List<Phone> phones;
    @CreationTimestamp
    private Date createdAt;
    @CreationTimestamp
    private Date lasModified;
    @CreationTimestamp
    private Date lasLogin;
    private Boolean isActive = true;

    public Boolean getActive() {
        return isActive;
    }

    public void setActive(Boolean active) {
        isActive = active;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getLasModified() {
        return lasModified;
    }

    public void setLasModified(Date lasModified) {
        this.lasModified = lasModified;
    }

    public Date getLasLogin() {
        return lasLogin;
    }

    public void setLasLogin(Date lasLogin) {
        this.lasLogin = lasLogin;
    }

    public User(String name, String email, String password, List<Phone> phones, Date createdAt, Date lasModified, Boolean isActive, Date lasLogin) {
        this.name = name;
        this.email = email;
        this.password = password;
        this.phones = phones;
        this.createdAt = createdAt;
        this.lasModified = lasModified;
        this.isActive = isActive;
        this.lasLogin = lasLogin;
    }

    public User() {}

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }


    public List<Phone> getPhones() {
        return phones;
    }

    public void setPhones(List<Phone> phones) {
        this.phones = phones;
    }
}
