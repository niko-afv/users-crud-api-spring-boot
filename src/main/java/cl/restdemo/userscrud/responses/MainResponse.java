package cl.restdemo.userscrud.responses;


public class MainResponse {

    private String message;
    private boolean hasError;
    private Integer code;

    public MainResponse(String message, boolean hasError, Integer code) {
        this.message = message;
        this.hasError = hasError;
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean getHasError() {
        return hasError;
    }

    public void setHasError(boolean hasError) {
        this.hasError = hasError;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }
}
