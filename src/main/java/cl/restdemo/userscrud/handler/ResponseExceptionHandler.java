package cl.restdemo.userscrud.handler;


import cl.restdemo.userscrud.exceptions.EmailAlreadyExistsException;
import cl.restdemo.userscrud.responses.MainResponse;
import org.springframework.http.HttpStatus;
import org.springframework.http.HttpStatusCode;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class ResponseExceptionHandler  extends ResponseEntityExceptionHandler {


    @ExceptionHandler(value = EmailAlreadyExistsException.class)
    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    protected ResponseEntity<MainResponse> handleEmailAlreadyExistsException(EmailAlreadyExistsException ex){
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new MainResponse("Email ya Existe", true, HttpStatus.BAD_REQUEST.value()));
    }
}
